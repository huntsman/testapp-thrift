﻿namespace TestApp
{
    partial class SelectType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.rab_Pmp = new System.Windows.Forms.RadioButton();
            this.rab_Lls = new System.Windows.Forms.RadioButton();
            this.button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Выберите тип уровнемера ";
            // 
            // rab_Pmp
            // 
            this.rab_Pmp.AutoSize = true;
            this.rab_Pmp.Location = new System.Drawing.Point(15, 29);
            this.rab_Pmp.Name = "rab_Pmp";
            this.rab_Pmp.Size = new System.Drawing.Size(48, 17);
            this.rab_Pmp.TabIndex = 1;
            this.rab_Pmp.TabStop = true;
            this.rab_Pmp.Text = "PMP";
            this.rab_Pmp.UseVisualStyleBackColor = true;
            // 
            // rab_Lls
            // 
            this.rab_Lls.AutoSize = true;
            this.rab_Lls.Location = new System.Drawing.Point(15, 52);
            this.rab_Lls.Name = "rab_Lls";
            this.rab_Lls.Size = new System.Drawing.Size(44, 17);
            this.rab_Lls.TabIndex = 2;
            this.rab_Lls.TabStop = true;
            this.rab_Lls.Text = "LLS";
            this.rab_Lls.UseVisualStyleBackColor = true;
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(75, 46);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(75, 23);
            this.button.TabIndex = 3;
            this.button.Text = "Ок";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // SelectType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(162, 81);
            this.ControlBox = false;
            this.Controls.Add(this.button);
            this.Controls.Add(this.rab_Lls);
            this.Controls.Add(this.rab_Pmp);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "SelectType";
            this.Text = "SelectType";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rab_Pmp;
        private System.Windows.Forms.RadioButton rab_Lls;
        private System.Windows.Forms.Button button;
    }
}