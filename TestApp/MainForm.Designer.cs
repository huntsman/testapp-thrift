﻿namespace TestApp
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.StartMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lab_Address1 = new System.Windows.Forms.Label();
            this.tbx_address1 = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cbx_active1 = new System.Windows.Forms.CheckBox();
            this.lab_Weight1 = new System.Windows.Forms.Label();
            this.tbx_Weight1 = new System.Windows.Forms.TextBox();
            this.lab_Filling1 = new System.Windows.Forms.Label();
            this.tbx_Filling1 = new System.Windows.Forms.TextBox();
            this.lab_Density1 = new System.Windows.Forms.Label();
            this.tbx_Density1 = new System.Windows.Forms.TextBox();
            this.lab_TotalVolume1 = new System.Windows.Forms.Label();
            this.tbx_TotalVolume1 = new System.Windows.Forms.TextBox();
            this.lab_UnderLiqLevel1 = new System.Windows.Forms.Label();
            this.tbx_UnderLiqLevel1 = new System.Windows.Forms.TextBox();
            this.lab_MainCorkLevel1 = new System.Windows.Forms.Label();
            this.tbx_MainCorkLevel1 = new System.Windows.Forms.TextBox();
            this.lab_VolOfMainProd1 = new System.Windows.Forms.Label();
            this.tbx_VolOfMainProd1 = new System.Windows.Forms.TextBox();
            this.lab_AverageTemp1 = new System.Windows.Forms.Label();
            this.tbx_AverageTemp1 = new System.Windows.Forms.TextBox();
            this.cbx_active2 = new System.Windows.Forms.CheckBox();
            this.lab_Weight2 = new System.Windows.Forms.Label();
            this.tbx_Weight2 = new System.Windows.Forms.TextBox();
            this.lab_Filling2 = new System.Windows.Forms.Label();
            this.tbx_Filling2 = new System.Windows.Forms.TextBox();
            this.lab_Density2 = new System.Windows.Forms.Label();
            this.tbx_Density2 = new System.Windows.Forms.TextBox();
            this.lab_TotalVolume2 = new System.Windows.Forms.Label();
            this.tbx_TotalVolume2 = new System.Windows.Forms.TextBox();
            this.lab_UnderLiqLevel2 = new System.Windows.Forms.Label();
            this.tbx_UnderLiqLevel2 = new System.Windows.Forms.TextBox();
            this.lab_MainCorkLevel2 = new System.Windows.Forms.Label();
            this.tbx_MainCorkLevel2 = new System.Windows.Forms.TextBox();
            this.lab_VolOfMainProd2 = new System.Windows.Forms.Label();
            this.tbx_VolOfMainProd2 = new System.Windows.Forms.TextBox();
            this.lab_AverageTemp2 = new System.Windows.Forms.Label();
            this.tbx_AverageTemp2 = new System.Windows.Forms.TextBox();
            this.lab_Address2 = new System.Windows.Forms.Label();
            this.tbx_address2 = new System.Windows.Forms.TextBox();
            this.mainButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StartMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(320, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // StartMenuItem
            // 
            this.StartMenuItem.Name = "StartMenuItem";
            this.StartMenuItem.Size = new System.Drawing.Size(58, 20);
            this.StartMenuItem.Text = "Начать";
            this.StartMenuItem.Click += new System.EventHandler(this.StartMenuItem_Click);
            // 
            // lab_Address1
            // 
            this.lab_Address1.AutoSize = true;
            this.lab_Address1.Enabled = false;
            this.lab_Address1.Location = new System.Drawing.Point(12, 9);
            this.lab_Address1.Name = "lab_Address1";
            this.lab_Address1.Size = new System.Drawing.Size(38, 13);
            this.lab_Address1.TabIndex = 1;
            this.lab_Address1.Text = "Адрес";
            // 
            // tbx_address1
            // 
            this.tbx_address1.Enabled = false;
            this.tbx_address1.Location = new System.Drawing.Point(15, 25);
            this.tbx_address1.Name = "tbx_address1";
            this.tbx_address1.Size = new System.Drawing.Size(100, 20);
            this.tbx_address1.TabIndex = 2;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(0, 27);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.cbx_active1);
            this.splitContainer1.Panel1.Controls.Add(this.lab_Weight1);
            this.splitContainer1.Panel1.Controls.Add(this.tbx_Weight1);
            this.splitContainer1.Panel1.Controls.Add(this.lab_Filling1);
            this.splitContainer1.Panel1.Controls.Add(this.tbx_Filling1);
            this.splitContainer1.Panel1.Controls.Add(this.lab_Density1);
            this.splitContainer1.Panel1.Controls.Add(this.tbx_Density1);
            this.splitContainer1.Panel1.Controls.Add(this.lab_TotalVolume1);
            this.splitContainer1.Panel1.Controls.Add(this.tbx_TotalVolume1);
            this.splitContainer1.Panel1.Controls.Add(this.lab_UnderLiqLevel1);
            this.splitContainer1.Panel1.Controls.Add(this.tbx_UnderLiqLevel1);
            this.splitContainer1.Panel1.Controls.Add(this.lab_MainCorkLevel1);
            this.splitContainer1.Panel1.Controls.Add(this.tbx_MainCorkLevel1);
            this.splitContainer1.Panel1.Controls.Add(this.lab_VolOfMainProd1);
            this.splitContainer1.Panel1.Controls.Add(this.tbx_VolOfMainProd1);
            this.splitContainer1.Panel1.Controls.Add(this.lab_AverageTemp1);
            this.splitContainer1.Panel1.Controls.Add(this.tbx_AverageTemp1);
            this.splitContainer1.Panel1.Controls.Add(this.lab_Address1);
            this.splitContainer1.Panel1.Controls.Add(this.tbx_address1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.cbx_active2);
            this.splitContainer1.Panel2.Controls.Add(this.lab_Weight2);
            this.splitContainer1.Panel2.Controls.Add(this.tbx_Weight2);
            this.splitContainer1.Panel2.Controls.Add(this.lab_Filling2);
            this.splitContainer1.Panel2.Controls.Add(this.tbx_Filling2);
            this.splitContainer1.Panel2.Controls.Add(this.lab_Density2);
            this.splitContainer1.Panel2.Controls.Add(this.tbx_Density2);
            this.splitContainer1.Panel2.Controls.Add(this.lab_TotalVolume2);
            this.splitContainer1.Panel2.Controls.Add(this.tbx_TotalVolume2);
            this.splitContainer1.Panel2.Controls.Add(this.lab_UnderLiqLevel2);
            this.splitContainer1.Panel2.Controls.Add(this.tbx_UnderLiqLevel2);
            this.splitContainer1.Panel2.Controls.Add(this.lab_MainCorkLevel2);
            this.splitContainer1.Panel2.Controls.Add(this.tbx_MainCorkLevel2);
            this.splitContainer1.Panel2.Controls.Add(this.lab_VolOfMainProd2);
            this.splitContainer1.Panel2.Controls.Add(this.tbx_VolOfMainProd2);
            this.splitContainer1.Panel2.Controls.Add(this.lab_AverageTemp2);
            this.splitContainer1.Panel2.Controls.Add(this.tbx_AverageTemp2);
            this.splitContainer1.Panel2.Controls.Add(this.lab_Address2);
            this.splitContainer1.Panel2.Controls.Add(this.tbx_address2);
            this.splitContainer1.Panel2Collapsed = true;
            this.splitContainer1.Size = new System.Drawing.Size(320, 219);
            this.splitContainer1.SplitterDistance = 25;
            this.splitContainer1.TabIndex = 3;
            // 
            // cbx_active1
            // 
            this.cbx_active1.AutoSize = true;
            this.cbx_active1.Enabled = false;
            this.cbx_active1.Location = new System.Drawing.Point(203, 25);
            this.cbx_active1.Name = "cbx_active1";
            this.cbx_active1.Size = new System.Drawing.Size(68, 17);
            this.cbx_active1.TabIndex = 19;
            this.cbx_active1.Text = "Активен";
            this.cbx_active1.UseVisualStyleBackColor = true;
            // 
            // lab_Weight1
            // 
            this.lab_Weight1.AutoSize = true;
            this.lab_Weight1.Enabled = false;
            this.lab_Weight1.Location = new System.Drawing.Point(200, 181);
            this.lab_Weight1.Name = "lab_Weight1";
            this.lab_Weight1.Size = new System.Drawing.Size(51, 13);
            this.lab_Weight1.TabIndex = 17;
            this.lab_Weight1.Text = "Масса, т";
            // 
            // tbx_Weight1
            // 
            this.tbx_Weight1.Enabled = false;
            this.tbx_Weight1.Location = new System.Drawing.Point(203, 197);
            this.tbx_Weight1.Name = "tbx_Weight1";
            this.tbx_Weight1.Size = new System.Drawing.Size(100, 20);
            this.tbx_Weight1.TabIndex = 18;
            // 
            // lab_Filling1
            // 
            this.lab_Filling1.AutoSize = true;
            this.lab_Filling1.Enabled = false;
            this.lab_Filling1.Location = new System.Drawing.Point(200, 138);
            this.lab_Filling1.Name = "lab_Filling1";
            this.lab_Filling1.Size = new System.Drawing.Size(82, 13);
            this.lab_Filling1.TabIndex = 15;
            this.lab_Filling1.Text = "Заполнение, %";
            // 
            // tbx_Filling1
            // 
            this.tbx_Filling1.Enabled = false;
            this.tbx_Filling1.Location = new System.Drawing.Point(203, 154);
            this.tbx_Filling1.Name = "tbx_Filling1";
            this.tbx_Filling1.Size = new System.Drawing.Size(100, 20);
            this.tbx_Filling1.TabIndex = 16;
            // 
            // lab_Density1
            // 
            this.lab_Density1.AutoSize = true;
            this.lab_Density1.Enabled = false;
            this.lab_Density1.Location = new System.Drawing.Point(200, 95);
            this.lab_Density1.Name = "lab_Density1";
            this.lab_Density1.Size = new System.Drawing.Size(91, 13);
            this.lab_Density1.TabIndex = 13;
            this.lab_Density1.Text = "Плотность, т/м3";
            // 
            // tbx_Density1
            // 
            this.tbx_Density1.Enabled = false;
            this.tbx_Density1.Location = new System.Drawing.Point(203, 111);
            this.tbx_Density1.Name = "tbx_Density1";
            this.tbx_Density1.Size = new System.Drawing.Size(100, 20);
            this.tbx_Density1.TabIndex = 14;
            // 
            // lab_TotalVolume1
            // 
            this.lab_TotalVolume1.AutoSize = true;
            this.lab_TotalVolume1.Enabled = false;
            this.lab_TotalVolume1.Location = new System.Drawing.Point(200, 52);
            this.lab_TotalVolume1.Name = "lab_TotalVolume1";
            this.lab_TotalVolume1.Size = new System.Drawing.Size(49, 13);
            this.lab_TotalVolume1.TabIndex = 11;
            this.lab_TotalVolume1.Text = "Частота";
            // 
            // tbx_TotalVolume1
            // 
            this.tbx_TotalVolume1.Enabled = false;
            this.tbx_TotalVolume1.Location = new System.Drawing.Point(203, 68);
            this.tbx_TotalVolume1.Name = "tbx_TotalVolume1";
            this.tbx_TotalVolume1.Size = new System.Drawing.Size(100, 20);
            this.tbx_TotalVolume1.TabIndex = 12;
            // 
            // lab_UnderLiqLevel1
            // 
            this.lab_UnderLiqLevel1.AutoSize = true;
            this.lab_UnderLiqLevel1.Enabled = false;
            this.lab_UnderLiqLevel1.Location = new System.Drawing.Point(12, 181);
            this.lab_UnderLiqLevel1.Name = "lab_UnderLiqLevel1";
            this.lab_UnderLiqLevel1.Size = new System.Drawing.Size(185, 13);
            this.lab_UnderLiqLevel1.TabIndex = 9;
            this.lab_UnderLiqLevel1.Text = "Уровень подтоварной жидкости, м";
            // 
            // tbx_UnderLiqLevel1
            // 
            this.tbx_UnderLiqLevel1.Enabled = false;
            this.tbx_UnderLiqLevel1.Location = new System.Drawing.Point(15, 197);
            this.tbx_UnderLiqLevel1.Name = "tbx_UnderLiqLevel1";
            this.tbx_UnderLiqLevel1.Size = new System.Drawing.Size(100, 20);
            this.tbx_UnderLiqLevel1.TabIndex = 10;
            // 
            // lab_MainCorkLevel1
            // 
            this.lab_MainCorkLevel1.AutoSize = true;
            this.lab_MainCorkLevel1.Enabled = false;
            this.lab_MainCorkLevel1.Location = new System.Drawing.Point(12, 138);
            this.lab_MainCorkLevel1.Name = "lab_MainCorkLevel1";
            this.lab_MainCorkLevel1.Size = new System.Drawing.Size(172, 13);
            this.lab_MainCorkLevel1.TabIndex = 7;
            this.lab_MainCorkLevel1.Text = "Уровень основного поплавка, м";
            // 
            // tbx_MainCorkLevel1
            // 
            this.tbx_MainCorkLevel1.Enabled = false;
            this.tbx_MainCorkLevel1.Location = new System.Drawing.Point(15, 154);
            this.tbx_MainCorkLevel1.Name = "tbx_MainCorkLevel1";
            this.tbx_MainCorkLevel1.Size = new System.Drawing.Size(100, 20);
            this.tbx_MainCorkLevel1.TabIndex = 8;
            // 
            // lab_VolOfMainProd1
            // 
            this.lab_VolOfMainProd1.AutoSize = true;
            this.lab_VolOfMainProd1.Enabled = false;
            this.lab_VolOfMainProd1.Location = new System.Drawing.Point(12, 95);
            this.lab_VolOfMainProd1.Name = "lab_VolOfMainProd1";
            this.lab_VolOfMainProd1.Size = new System.Drawing.Size(153, 13);
            this.lab_VolOfMainProd1.TabIndex = 5;
            this.lab_VolOfMainProd1.Text = "Относительный уровень, мм";
            // 
            // tbx_VolOfMainProd1
            // 
            this.tbx_VolOfMainProd1.Enabled = false;
            this.tbx_VolOfMainProd1.Location = new System.Drawing.Point(15, 111);
            this.tbx_VolOfMainProd1.Name = "tbx_VolOfMainProd1";
            this.tbx_VolOfMainProd1.Size = new System.Drawing.Size(100, 20);
            this.tbx_VolOfMainProd1.TabIndex = 6;
            // 
            // lab_AverageTemp1
            // 
            this.lab_AverageTemp1.AutoSize = true;
            this.lab_AverageTemp1.Enabled = false;
            this.lab_AverageTemp1.Location = new System.Drawing.Point(12, 52);
            this.lab_AverageTemp1.Name = "lab_AverageTemp1";
            this.lab_AverageTemp1.Size = new System.Drawing.Size(131, 13);
            this.lab_AverageTemp1.TabIndex = 3;
            this.lab_AverageTemp1.Text = "Средняя температура, С";
            // 
            // tbx_AverageTemp1
            // 
            this.tbx_AverageTemp1.Enabled = false;
            this.tbx_AverageTemp1.Location = new System.Drawing.Point(15, 68);
            this.tbx_AverageTemp1.Name = "tbx_AverageTemp1";
            this.tbx_AverageTemp1.Size = new System.Drawing.Size(100, 20);
            this.tbx_AverageTemp1.TabIndex = 4;
            // 
            // cbx_active2
            // 
            this.cbx_active2.AutoSize = true;
            this.cbx_active2.Location = new System.Drawing.Point(200, 24);
            this.cbx_active2.Name = "cbx_active2";
            this.cbx_active2.Size = new System.Drawing.Size(68, 17);
            this.cbx_active2.TabIndex = 38;
            this.cbx_active2.Text = "Активен";
            this.cbx_active2.UseVisualStyleBackColor = true;
            // 
            // lab_Weight2
            // 
            this.lab_Weight2.AutoSize = true;
            this.lab_Weight2.Enabled = false;
            this.lab_Weight2.Location = new System.Drawing.Point(197, 180);
            this.lab_Weight2.Name = "lab_Weight2";
            this.lab_Weight2.Size = new System.Drawing.Size(51, 13);
            this.lab_Weight2.TabIndex = 36;
            this.lab_Weight2.Text = "Масса, т";
            // 
            // tbx_Weight2
            // 
            this.tbx_Weight2.Enabled = false;
            this.tbx_Weight2.Location = new System.Drawing.Point(200, 196);
            this.tbx_Weight2.Name = "tbx_Weight2";
            this.tbx_Weight2.Size = new System.Drawing.Size(100, 20);
            this.tbx_Weight2.TabIndex = 37;
            // 
            // lab_Filling2
            // 
            this.lab_Filling2.AutoSize = true;
            this.lab_Filling2.Enabled = false;
            this.lab_Filling2.Location = new System.Drawing.Point(197, 137);
            this.lab_Filling2.Name = "lab_Filling2";
            this.lab_Filling2.Size = new System.Drawing.Size(82, 13);
            this.lab_Filling2.TabIndex = 34;
            this.lab_Filling2.Text = "Заполнение, %";
            // 
            // tbx_Filling2
            // 
            this.tbx_Filling2.Enabled = false;
            this.tbx_Filling2.Location = new System.Drawing.Point(200, 153);
            this.tbx_Filling2.Name = "tbx_Filling2";
            this.tbx_Filling2.Size = new System.Drawing.Size(100, 20);
            this.tbx_Filling2.TabIndex = 35;
            // 
            // lab_Density2
            // 
            this.lab_Density2.AutoSize = true;
            this.lab_Density2.Enabled = false;
            this.lab_Density2.Location = new System.Drawing.Point(197, 94);
            this.lab_Density2.Name = "lab_Density2";
            this.lab_Density2.Size = new System.Drawing.Size(91, 13);
            this.lab_Density2.TabIndex = 32;
            this.lab_Density2.Text = "Плотность, т/м3";
            // 
            // tbx_Density2
            // 
            this.tbx_Density2.Enabled = false;
            this.tbx_Density2.Location = new System.Drawing.Point(200, 110);
            this.tbx_Density2.Name = "tbx_Density2";
            this.tbx_Density2.Size = new System.Drawing.Size(100, 20);
            this.tbx_Density2.TabIndex = 33;
            // 
            // lab_TotalVolume2
            // 
            this.lab_TotalVolume2.AutoSize = true;
            this.lab_TotalVolume2.Location = new System.Drawing.Point(197, 51);
            this.lab_TotalVolume2.Name = "lab_TotalVolume2";
            this.lab_TotalVolume2.Size = new System.Drawing.Size(49, 13);
            this.lab_TotalVolume2.TabIndex = 30;
            this.lab_TotalVolume2.Text = "Частота";
            // 
            // tbx_TotalVolume2
            // 
            this.tbx_TotalVolume2.Location = new System.Drawing.Point(200, 67);
            this.tbx_TotalVolume2.Name = "tbx_TotalVolume2";
            this.tbx_TotalVolume2.Size = new System.Drawing.Size(100, 20);
            this.tbx_TotalVolume2.TabIndex = 31;
            // 
            // lab_UnderLiqLevel2
            // 
            this.lab_UnderLiqLevel2.AutoSize = true;
            this.lab_UnderLiqLevel2.Enabled = false;
            this.lab_UnderLiqLevel2.Location = new System.Drawing.Point(9, 180);
            this.lab_UnderLiqLevel2.Name = "lab_UnderLiqLevel2";
            this.lab_UnderLiqLevel2.Size = new System.Drawing.Size(185, 13);
            this.lab_UnderLiqLevel2.TabIndex = 28;
            this.lab_UnderLiqLevel2.Text = "Уровень подтоварной жидкости, м";
            // 
            // tbx_UnderLiqLevel2
            // 
            this.tbx_UnderLiqLevel2.Enabled = false;
            this.tbx_UnderLiqLevel2.Location = new System.Drawing.Point(12, 196);
            this.tbx_UnderLiqLevel2.Name = "tbx_UnderLiqLevel2";
            this.tbx_UnderLiqLevel2.Size = new System.Drawing.Size(100, 20);
            this.tbx_UnderLiqLevel2.TabIndex = 29;
            // 
            // lab_MainCorkLevel2
            // 
            this.lab_MainCorkLevel2.AutoSize = true;
            this.lab_MainCorkLevel2.Enabled = false;
            this.lab_MainCorkLevel2.Location = new System.Drawing.Point(9, 137);
            this.lab_MainCorkLevel2.Name = "lab_MainCorkLevel2";
            this.lab_MainCorkLevel2.Size = new System.Drawing.Size(172, 13);
            this.lab_MainCorkLevel2.TabIndex = 26;
            this.lab_MainCorkLevel2.Text = "Уровень основного поплавка, м";
            // 
            // tbx_MainCorkLevel2
            // 
            this.tbx_MainCorkLevel2.Enabled = false;
            this.tbx_MainCorkLevel2.Location = new System.Drawing.Point(12, 153);
            this.tbx_MainCorkLevel2.Name = "tbx_MainCorkLevel2";
            this.tbx_MainCorkLevel2.Size = new System.Drawing.Size(100, 20);
            this.tbx_MainCorkLevel2.TabIndex = 27;
            // 
            // lab_VolOfMainProd2
            // 
            this.lab_VolOfMainProd2.AutoSize = true;
            this.lab_VolOfMainProd2.Location = new System.Drawing.Point(9, 94);
            this.lab_VolOfMainProd2.Name = "lab_VolOfMainProd2";
            this.lab_VolOfMainProd2.Size = new System.Drawing.Size(153, 13);
            this.lab_VolOfMainProd2.TabIndex = 24;
            this.lab_VolOfMainProd2.Text = "Относительный уровень, мм";
            // 
            // tbx_VolOfMainProd2
            // 
            this.tbx_VolOfMainProd2.Location = new System.Drawing.Point(12, 110);
            this.tbx_VolOfMainProd2.Name = "tbx_VolOfMainProd2";
            this.tbx_VolOfMainProd2.Size = new System.Drawing.Size(100, 20);
            this.tbx_VolOfMainProd2.TabIndex = 25;
            // 
            // lab_AverageTemp2
            // 
            this.lab_AverageTemp2.AutoSize = true;
            this.lab_AverageTemp2.Location = new System.Drawing.Point(9, 51);
            this.lab_AverageTemp2.Name = "lab_AverageTemp2";
            this.lab_AverageTemp2.Size = new System.Drawing.Size(131, 13);
            this.lab_AverageTemp2.TabIndex = 22;
            this.lab_AverageTemp2.Text = "Средняя температура, С";
            // 
            // tbx_AverageTemp2
            // 
            this.tbx_AverageTemp2.Location = new System.Drawing.Point(12, 67);
            this.tbx_AverageTemp2.Name = "tbx_AverageTemp2";
            this.tbx_AverageTemp2.Size = new System.Drawing.Size(100, 20);
            this.tbx_AverageTemp2.TabIndex = 23;
            // 
            // lab_Address2
            // 
            this.lab_Address2.AutoSize = true;
            this.lab_Address2.Location = new System.Drawing.Point(9, 8);
            this.lab_Address2.Name = "lab_Address2";
            this.lab_Address2.Size = new System.Drawing.Size(38, 13);
            this.lab_Address2.TabIndex = 20;
            this.lab_Address2.Text = "Адрес";
            // 
            // tbx_address2
            // 
            this.tbx_address2.Location = new System.Drawing.Point(12, 24);
            this.tbx_address2.Name = "tbx_address2";
            this.tbx_address2.Size = new System.Drawing.Size(100, 20);
            this.tbx_address2.TabIndex = 21;
            // 
            // mainButton
            // 
            this.mainButton.Enabled = false;
            this.mainButton.Location = new System.Drawing.Point(122, 264);
            this.mainButton.Name = "mainButton";
            this.mainButton.Size = new System.Drawing.Size(75, 23);
            this.mainButton.TabIndex = 4;
            this.mainButton.Text = "Далее";
            this.mainButton.UseVisualStyleBackColor = true;
            this.mainButton.Click += new System.EventHandler(this.mainButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 299);
            this.Controls.Add(this.mainButton);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "TestApp";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem StartMenuItem;
        private System.Windows.Forms.Label lab_Address1;
        private System.Windows.Forms.TextBox tbx_address1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.CheckBox cbx_active1;
        private System.Windows.Forms.Label lab_Weight1;
        private System.Windows.Forms.TextBox tbx_Weight1;
        private System.Windows.Forms.Label lab_Filling1;
        private System.Windows.Forms.TextBox tbx_Filling1;
        private System.Windows.Forms.Label lab_Density1;
        private System.Windows.Forms.TextBox tbx_Density1;
        private System.Windows.Forms.Label lab_TotalVolume1;
        private System.Windows.Forms.TextBox tbx_TotalVolume1;
        private System.Windows.Forms.Label lab_UnderLiqLevel1;
        private System.Windows.Forms.TextBox tbx_UnderLiqLevel1;
        private System.Windows.Forms.Label lab_MainCorkLevel1;
        private System.Windows.Forms.TextBox tbx_MainCorkLevel1;
        private System.Windows.Forms.Label lab_VolOfMainProd1;
        private System.Windows.Forms.TextBox tbx_VolOfMainProd1;
        private System.Windows.Forms.Label lab_AverageTemp1;
        private System.Windows.Forms.TextBox tbx_AverageTemp1;
        private System.Windows.Forms.CheckBox cbx_active2;
        private System.Windows.Forms.Label lab_Weight2;
        private System.Windows.Forms.TextBox tbx_Weight2;
        private System.Windows.Forms.Label lab_Filling2;
        private System.Windows.Forms.TextBox tbx_Filling2;
        private System.Windows.Forms.Label lab_Density2;
        private System.Windows.Forms.TextBox tbx_Density2;
        private System.Windows.Forms.Label lab_TotalVolume2;
        private System.Windows.Forms.TextBox tbx_TotalVolume2;
        private System.Windows.Forms.Label lab_UnderLiqLevel2;
        private System.Windows.Forms.TextBox tbx_UnderLiqLevel2;
        private System.Windows.Forms.Label lab_MainCorkLevel2;
        private System.Windows.Forms.TextBox tbx_MainCorkLevel2;
        private System.Windows.Forms.Label lab_VolOfMainProd2;
        private System.Windows.Forms.TextBox tbx_VolOfMainProd2;
        private System.Windows.Forms.Label lab_AverageTemp2;
        private System.Windows.Forms.TextBox tbx_AverageTemp2;
        private System.Windows.Forms.Label lab_Address2;
        private System.Windows.Forms.TextBox tbx_address2;
        private System.Windows.Forms.Button mainButton;
    }
}

