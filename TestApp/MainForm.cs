﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Thrift.Protocol;
using Thrift.Transport;

namespace TestApp
{
    public partial class MainForm : Form
    {
        private List<LevelInfo> Info;
        private Timer timer;
        private Type type;

        pmpEmulClient.Client emulClient;

        public MainForm()
        {
            InitializeComponent();
            Info = new List<LevelInfo>();
            timer = new Timer();
            timer.Interval = 500;
            timer.Tick += new EventHandler(this.timer_Tick);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            List<LevelInfo> info_update = new List<LevelInfo>();


            foreach (LevelInfo l_info in Info)
            {
                LevelInfo newli = new LevelInfo();
                newli.Addr = l_info.Addr;
                newli.Active = l_info.Active;
                l_info.AverageTemp += 10;
                newli.AverageTemp = l_info.AverageTemp;
                l_info.MainCorkLevel += 10;
                newli.MainCorkLevel = l_info.MainCorkLevel;
                info_update.Add(newli);
            }

            try
            {
                emulClient.update(info_update);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка");
            }
        }

        private void StartMenuItem_Click(object sender, EventArgs e)
        {
            SelectType TypeForm = new SelectType(this);
            TypeForm.Show();

        }

        bool finish = false;
        private void mainButton_Click(object sender, EventArgs e)
        {
            if (!finish)
            {
                try
                {
                    saveLevelGauge1();
                }
                catch
                {
                    MessageBox.Show("Введённые значения не являются допустимыми");
                    return;
                }

                splitContainer1.Panel1Collapsed = true;
                splitContainer1.Panel2Collapsed = false;
                mainButton.Text = "Настроить";
                lgField2Switch(true);
                finish = true;
            }
            else
            {
                try
                {
                    connect();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка соединения");
                    return;
                }
                try { 
                saveLevelGauge2();
                }
                catch
                {
                    MessageBox.Show("Введённые значения не являются допустимыми");
                    return;
                }
                mainButton.Enabled = false;
                lgField2Switch(false);

                try
                {
                    emulClient.setup(Info, type);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка");
                    return;
                }
                
                timer.Start();
            }
            
        }

        TcpClient tcpClient;
        TSocket tSocket;
        private void connect()
        {

            if (tcpClient != null)
            {
                emulClient.Dispose();
                tcpClient.Close();
                tSocket.Close();
                tSocket.Dispose();
            }

                tcpClient = new TcpClient();

                tcpClient.Connect("127.0.0.1", 9900);
                tSocket = new TSocket(tcpClient);
                TTransport transport = new TFramedTransport(tSocket);
                TProtocol proto = new TCompactProtocol(transport);


                emulClient = new pmpEmulClient.Client(proto);

            }

        private void saveLevelGauge1()
        {
            LevelInfo li = new LevelInfo();
            li.Active = cbx_active1.Checked;
            li.Addr = short.Parse(tbx_address1.Text);
            li.AverageTemp = double.Parse(tbx_AverageTemp1.Text);

            if (type == Type.LLS)
            {
                li.Filling = double.Parse(tbx_TotalVolume1.Text);
                li.MainCorkLevel = double.Parse(tbx_VolOfMainProd1.Text);
            }
            else
            {
                li.Density = double.Parse(tbx_Density1.Text);
                li.Filling = double.Parse(tbx_Filling1.Text);
                li.MainCorkLevel = double.Parse(tbx_MainCorkLevel1.Text);
                li.MainProdVol = double.Parse(tbx_VolOfMainProd1.Text);
                li.Mass = double.Parse(tbx_Weight1.Text);
                li.TotalCapacity = double.Parse(tbx_TotalVolume1.Text);
                li.UnderLiqLevel = double.Parse(tbx_UnderLiqLevel1.Text);
            }
            Info.Add(li);
        }

        private void saveLevelGauge2()
        {
            LevelInfo li = new LevelInfo();
            li.Active = cbx_active2.Checked;
            li.Addr = short.Parse(tbx_address2.Text);
            li.AverageTemp = double.Parse(tbx_AverageTemp2.Text);

            if (type == Type.LLS)
            {
                li.Filling = double.Parse(tbx_TotalVolume2.Text);
                li.MainCorkLevel = double.Parse(tbx_VolOfMainProd2.Text);
            }
            else
            {
                li.Density = double.Parse(tbx_Density2.Text);
                li.Filling = double.Parse(tbx_Filling2.Text);
                li.MainCorkLevel = double.Parse(tbx_MainCorkLevel2.Text);
                li.MainProdVol = double.Parse(tbx_VolOfMainProd2.Text);
                li.Mass = double.Parse(tbx_Weight2.Text);
                li.TotalCapacity = double.Parse(tbx_TotalVolume2.Text);
                li.UnderLiqLevel = double.Parse(tbx_UnderLiqLevel2.Text);
            }
            Info.Add(li);
        }

        public void startEditing(Type type)
        {
            this.type = type;

            splitContainer1.Panel1Collapsed = false;
            splitContainer1.Panel2Collapsed = true;
            lgField1Switch(false);
            lgField2Switch(false);
            mainButton.Text = "Далее";
            finish = false;
            timer.Stop();
            Info.Clear();

            if (type == Type.PMP)
            {
                lab_TotalVolume1.Text = "Общий объем, м3";
                lab_VolOfMainProd1.Text = "Объем основного продукта, м3";
                lab_TotalVolume2.Text = "Общий объем, м3";
                lab_VolOfMainProd2.Text = "Объем основного продукта, м3";

            }
            mainButton.Enabled = true;
            lgField1Switch(true);
        }

        private void lgField1Switch(bool enable)
        {
            cbx_active1.Enabled = enable;
            lab_Address1.Enabled = enable;
            tbx_address1.Enabled = enable;
            lab_AverageTemp1.Enabled = enable;
            tbx_AverageTemp1.Enabled = enable;
            lab_TotalVolume1.Enabled = enable;
            tbx_TotalVolume1.Enabled = enable;
            lab_VolOfMainProd1.Enabled = enable;
            tbx_VolOfMainProd1.Enabled = enable;

            if (type == Type.PMP)
            {
                lab_Weight1.Enabled = enable;
                tbx_Weight1.Enabled = enable;
                lab_Density1.Enabled = enable;
                tbx_Density1.Enabled = enable;
                lab_UnderLiqLevel1.Enabled = enable;
                tbx_UnderLiqLevel1.Enabled = enable;
                lab_MainCorkLevel1.Enabled = enable;
                tbx_MainCorkLevel1.Enabled = enable;
                lab_Filling1.Enabled = enable;
                tbx_Filling1.Enabled = enable;
            }
        }

        private void lgField2Switch(bool enable)
        {
            cbx_active2.Enabled = enable;
            lab_Address2.Enabled = enable;
            tbx_address2.Enabled = enable;
            lab_AverageTemp2.Enabled = enable;
            tbx_AverageTemp2.Enabled = enable;
            lab_TotalVolume2.Enabled = enable;
            tbx_TotalVolume2.Enabled = enable;
            lab_VolOfMainProd2.Enabled = enable;
            tbx_VolOfMainProd2.Enabled = enable;

            if (type == Type.PMP)
            {
                lab_Weight2.Enabled = enable;
                tbx_Weight2.Enabled = enable;
                lab_Density2.Enabled = enable;
                tbx_Density2.Enabled = enable;
                lab_UnderLiqLevel2.Enabled = enable;
                tbx_UnderLiqLevel2.Enabled = enable;
                lab_MainCorkLevel2.Enabled = enable;
                tbx_MainCorkLevel2.Enabled = enable;
                lab_Filling2.Enabled = enable;
                tbx_Filling2.Enabled = enable;
            }
        }

    }
}
