﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestApp
{
    public partial class SelectType : Form
    {
        private MainForm main;

        public SelectType(MainForm mf)
        {
            InitializeComponent();
            main = mf;
            rab_Pmp.Checked = true;
        }

        private void button_Click(object sender, EventArgs e)
        {
            if (rab_Lls.Checked)
            {
                main.startEditing(Type.LLS);
            }
            else
            {
                main.startEditing(Type.PMP);
            }
            this.Close();
        }
    }
}
